const express = require('express');
const app = express();
const fs = require('fs');
const port = 8080;
const filesRouter = require('./routers/filesRouter.js');
const morgan = require('morgan');

app.use(morgan('tiny'));
app.use(express.json());

app.use((err, req, res, next) => {
    if (err instanceof SyntaxError) {
        return res.status(400).json({'mesage':'Client error'});
    };
    next();
});

app.use('/api/files', filesRouter);

app.use('/', (req, res)=>{
    res.status(400);
    res.json({'mesage':'Client error'});
});

function start() {
    fs.mkdir('./files',(err)=>{
        if (!err){
            console.log('Filess directory creation successfully')
        } else {
            console.log('Files directory already exist');
        };
    });

    app.listen(port, ()=> {
        console.log(`Server works at port: ${port}`);
    });
};
start();