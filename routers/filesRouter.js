const express = require('express');
const router = express.Router();
const fs = require('fs');
const path = require('path');

const filesPath = './files/';

router.get('/',(req, res ) => {
    fs.readdir('./files',(err, files) => {
        if(err){
            res.status(500);
            res.json({'mesage':'Server error'})
        } else {
           res.status(200);
           res.json({'mesage':'Success','files':files}); 
        }
    });
});

router.get('/:filename', (req, res) => {
    const filename = req.params.filename.trim();
    try {
        let fileUploadDate = fs.statSync(`./files/${filename}`).birthtime;
        fs.readFile(`${filesPath}${filename}`,'utf8',(err, data) => {
        if (err) {
            throw err;
        } else {
            res.json({
                'message':'Success',
                'filename':filename,
                'content': data,
                'extension':path.extname(filename).substr(1),
                'uploadedDate':fileUploadDate
            });
        };
    });
    } catch (err) {
        res.status(400);
        res.json({'message':`No file with \'${filename}\' file name found`});
    }
});

router.post('/',(req, res) => {
    const fileName = req.body.filename;
    const fileData = req.body.content;
    const extReg = /\.(json|js|xml|txt|yaml|log)$/;
    if (!fileData || !fileName) {
        res.status(400);
        res.json({'mesage':'Please specify all parameters'});
        return;
    } else if (!extReg.test(path.extname(fileName))){
        res.status(400);
        res.json({'mesage':'Incorrect file extension'});
        return;
    };
    fs.writeFile(`${filesPath}${fileName}`,fileData,(err) => {
        if (err) {
            res.status(500);
            res.json({'mesage':'Server error'});
        } else {
            res.status(200);
            res.json({'mesage':'File created successfully'});
        }
    });
});

router.delete('/:filename', (req, res) => {
    const filename = req.params.filename.trim();
    fs.unlink(`${filesPath}${filename}`, (err) => {
        if (err) {
            res.status(400);
            res.json({'message':`No file with \'${filename}\' file name found`});
            return
        } else {
            res.status(200);
            res.json({'mesage':'File deleted successfully'});
        }
    });
});

module.exports = router;